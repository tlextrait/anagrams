/**
 * trietree.cpp
 *
 * Thomas Lextrait
 * thomas.lextrait@gmail.com
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "trietree.h"

/*
	Note: 	This is not the complete implementation of a TrieTree
			but a partial implementation sufficient for finding
			anagrams of words.
			For example the child nodes of a child are stored in an
			array, this allows to quickly figure out if a child
			exists but limits the ability to iterate through children.
			This means this implementation is not appropriate for
			applications such as autocompletion, but is appropriate
			for verifying the existence of words.
*/

/*
=================================================
TRIETREE
=================================================
*/

/**
* TrieTree constructor
*/
TrieTree::TrieTree()
{
	// The root is a node with NULL value and NULL parent
	root = new TTNode(NULL, false, NULL);

	cur_word = NULL;  // used for iteration
	cur_word_num = 0; // used for iteration
}

bool TrieTree::addBranch(char* word)
{
	if(word && !hasBranch(word)){
		int len = strlen(word);
		bool isWord;
		TTNode* curPos = root;
		for(int i=0; i<len; i++){
			isWord = i==len-1; // end of word
			bool added = curPos->addChild(word[i], isWord);
			curPos = curPos->getChild(word[i]);
			// stat purposes: count total nodes
			tt_total_nodes += added ? 1 : 0;
		}
		word_count++;
		return true;
	}else{
		return false;
	}
}

bool TrieTree::hasBranch(char* word)
{
	if(word){
		int len = strlen(word);
		TTNode* curPos = root;
		for(int i=0; i<len; i++){
			curPos = curPos->getChild(word[i]);
			if(!curPos){
				return false;
			}
		}
		return curPos->isEndOfWord();
	}else{
		return false;
	}
}

/**
* Returns the node for the last character of given word
* even if the node is not an end-of-word node
* @param word
* @return TTNode
*/
TTNode* TrieTree::getWordNode(char* word)
{
	if(word && strlen(word) > 0){
		TTNode* curNode = root;
		for(int i=0; i<strlen(word); i++){
			if(curNode->hasChild(word[i])){
				curNode = curNode->getChild(word[i]);
			}else{
				// Node doesn't exist
				return NULL;
			}
		}
		return curNode;
	}else{
		// null or zero length string means we want the root node
		return root;
	}
}

/**
* This is an iterator, returns the next word in the dictionary
* in alphabetical order
*/
char* TrieTree::getNextWord()
{
  if(!hasNextWord()){
    // There are no more words to return!
  	return NULL;
  }else{
  	if(!cur_word){

  		cur_word = new char[MAX_WORD_LENGTH]();

  		/*
			Strategy:
			We are guaranteed that all leaves of the TrieTree are
			the end of a word. All we need is to find the very first
			word we can, in alphabetical order. We simply go down
			the tree, going down the first child of first node...
			We stop as soon as we reach a node that's the end of a word.
			However this doesn't mean we reached maximum depth, some 
			longer words may start with the same radical.
  		*/

  		char* buffer = new char[MAX_WORD_LENGTH]();
  		int buffer_len = 0;
  		TTNode* curNode = root;

  		while(!curNode->isEndOfWord()){
  			for(int i=0; i <= curNode->getMaxChildIndex(); i++){
  				if(curNode->hasChild((char)i)){
  					// move on to next child
  					curNode = curNode->getChild((char)i);
  					// copy the character
  					buffer[buffer_len] = curNode->getValue();
  					buffer_len++;
  					if(curNode->isEndOfWord()){
  						break;
  					}
  				}
	  		}
  		}
  		
  		buffer[buffer_len] = '\0'; 	// append the termination char
  		strcpy(cur_word, buffer);	// mark as current word
  		cur_word_num++;

  		return cur_word;
  	}else{

  		/*
			Strategy:
			Look at current word, check if there are any children,
			if yes then take the first one, go down until end of word.
			If not, look at siblings. If there is one then go down its
			childrend until end of word.
			If no siblings or iterating is done, then go to second last
			character of current word, and go back to step 1 using
			recursion.
  		*/

		TTNode* curNode = getWordNode(cur_word);
		bool next_found = false;

		if(curNode){

			int cur_len = strlen(cur_word);

			while(!next_found || !curNode->isEndOfWord()){
				if(!curNode->hasChildren() && cur_len > 0){
					// trim last char of word, to go up one parent
					char last = cur_word[cur_len-1];
					cur_word[cur_len-1] = '\0';
					cur_len--;
					// fetch the node
					curNode = curNode->getParent();
					while(!curNode->hasNextChild(last)){
						// trim last char of word, to go up one parent
						last = cur_word[cur_len-1];
						cur_word[cur_len-1] = '\0';
						cur_len--;
						// fetch the node
						if(curNode->hasParent()){
							curNode = curNode->getParent();
						}else{
							break;
						}
					}
					curNode = curNode->getNextChild(last);
				}else{
					// go down until we find an end-of-word
					curNode = curNode->getFirstChild();
					cur_word[cur_len] = curNode->getValue();
					cur_len++;
				}
				if(curNode->isEndOfWord()) next_found = true;
			}

			// Copy the current node's word into cur_word
			delete cur_word;
			cur_word = new char[MAX_WORD_LENGTH]();
			cur_word[cur_len+1] = '\0';
			while(curNode->hasParent() && cur_len >= 0){
				char value = curNode->getValue();
				if(value) cur_word[cur_len] = value;
				cur_len--;
				curNode = curNode->getParent();
			}

			cur_word_num++;

			return cur_word;
		}else{
			// We have an error, should reset the iterator
			fprintf(stderr, "Error occured with the tree iterator\n");
			return NULL;
		}
  	}
  }
}

/**
* Indicates if there's a next word available in the iterator
*/
bool TrieTree::hasNextWord()
{
	return cur_word_num < word_count && word_count > 0;
}

/**
* Resets the iterator
*/
void TrieTree::resetIterator()
{
  if(cur_word) delete cur_word;
  cur_word = NULL;
  cur_word_num = 0;
}

/*
=================================================
TRIETREE NODE
=================================================
*/

/**
* TrieTreeNode constructor
* @param character
*/
TTNode::TTNode(char c, bool isWordEndNode, TTNode* parent)
{
	// convert to lower case
	value = lowerCase(c);
	isWord = isWordEndNode;
	children = new TTNode*[MAX_CHARSET](); // parenthesis initialize the array
	child_count = 0;
	max_child_index = -1;
	parentNode = parent;
}

/**
* Creates a TrieTreeNode pointer and return it
* @param character
* @return TTNode
*/
TTNode* TTNode::makeNode(char c, bool isWordEndNode, TTNode* parent)
{
	// convert to lower case
	c = lowerCase(c);

	TTNode* newNode = new TTNode(c, isWordEndNode, parent);
	return newNode;
}

/**
* Indicates if this node has children
* @return boolean
*/
bool TTNode::hasChildren()
{
	return child_count > 0;
}

/**
* Returns the largest child index
* @return integer
*/
int TTNode::getMaxChildIndex()
{
	return max_child_index;
}

/**
* Indicates if this node has the given child
* @param character
* @return boolean
*/
bool TTNode::hasChild(char c)
{
	// Ensure it's in the CHARSET for this dictionary
	if((int)c <= MAX_CHARSET){
		// convert to lower case
		c = lowerCase(c);
		return children[(int)c] != NULL && children[(int)c]->getValue() == c;
	}else{
		return false;
	}
}

/**
* Adds a new child to this node
* @param character
* @return boolean indicated if the child already existed
*/
bool TTNode::addChild(char c, bool isWordEndNode)
{
	// convert to lower case
	c = lowerCase(c);

	if(
		!hasChild(c) || 
		(children[(int)c] && !children[(int)c]->isEndOfWord() && isWordEndNode)
	){

		// If the child already exists then free memory
		if(children[(int)c]){
			delete children[(int)c];
			children[(int)c] = NULL;
		}
		// Store the new node
		children[(int)c] = TTNode::makeNode(c, isWordEndNode, this);
		// Increment number of children
		child_count++;
		// Record the index?
		if((int)c > max_child_index) max_child_index = (int)c;
		return true;
	}else{
		return false;
	}
}

/**
* Indicates if this node is the end of a word
* @return boolean
*/
bool TTNode::isEndOfWord()
{
	return isWord;
}

/**
* Returns the character stored by this node
*/
char TTNode::getValue()
{
	return value;
}

/**
* Returns the child requested, or NULL if it doesn't exist
* @param character
* @return TTNode*
*/
TTNode* TTNode::getChild(char c)
{
	// convert to lower case
	c = lowerCase(c);

	if(hasChild(c)){
		return children[(int)c];
	}else{
		return NULL;
	}
}

/**
* Returns the child after this one
* @param character
* @return TTNode*
*/
TTNode* TTNode::getNextChild(char c)
{
	if(hasNextChild(c)){
		for(int i = ((int)c)+1; i <= getMaxChildIndex(); i++){
			if(hasChild((char)i)){
				return getChild((char)i);
			}
		}
		return NULL;
	}else{
		return NULL;
	}
}

/**
* Returns the first child we can find
* @return TTNode*
*/
TTNode* TTNode::getFirstChild()
{
	if(hasChildren()){
		// return the very first child we can find
		for(int i = 0; i <= getMaxChildIndex(); i++){
			if(hasChild((char)i)){
				return getChild((char)i);
			}
		}
		return NULL;
	}else{
		return NULL;
	}
}

/**
* Indicates if the node has a children after the one given
* @param character
* @return boolean
*/
bool TTNode::hasNextChild(char c)
{
	return c && c!='\0' && hasChildren() && getMaxChildIndex() > (int)c;
}

/**
* Returns the parent of this node
* @return TTNode*
*/
TTNode* TTNode::getParent()
{
	return parentNode;
}

/**
* Indicates if the node has a parent
*/
bool TTNode::hasParent()
{
	return parentNode!=NULL;
}

/**
* Converts a char to lower case
* @param character
* @return lower case character
*/
char lowerCase(char c)
{
	int ascii = (int)c;
	if(ascii >= 65 && ascii <= 90){
		ascii += 32;
		c = (char)ascii;
	}
	return c;
}

/**
* Converts a word to lower case
* @param word
* @return lower case word
*/
char* lowerCaseWord(char* word)
{
	for(int i=0; i<strlen(word); i++){
		word[i] = lowerCase(word[i]);
	}
	return word;
}
