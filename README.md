Anagrams is a C++ program that returns all English word anagrams for given words.
author:	Thomas Lextrait, thomas.lextrait@gmail.com, Feb 2014
compile: make new
usage:	./anagrams [-h] [-s] [-w] [x] WORDS...
	-h	Display help
	-s	Display statistics for each word
	-w	Count and display the number of words in the dictionary
	-x	Display big O information
