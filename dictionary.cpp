/**
 * dictionary.cpp
 *
 * Thomas Lextrait
 * thomas.lextrait@gmail.com
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// File opening with C++
#include <iostream>
#include <fstream>

#include "dictionary.h"

/**
* Dictionary constructor
*/
Dictionary::Dictionary()
{
  word_count = 0;
  TT = new TrieTree();
}

/**
* Build the dictionary from file references in macro: DICTIONARY_FILE
* - see dictionary.h for the macro
*/
void Dictionary::buildFromFile()
{
  // Erase tree?
  if(TT) delete TT;
  TT = new TrieTree();

  std::string line;
  std::ifstream myfile(DICTIONARY_FILE);
  if(myfile.is_open()){
    while(getline(myfile, line)){
      // Convert the word to char*
      char *cstr = new char[line.length() + 1];
      strcpy(cstr, line.c_str());
      addWord(cstr);
    }
    myfile.close();
  }
}

/**
* Adds a word to the dictionary
*/
bool Dictionary::addWord(char* word)
{
  if(word && strlen(word) > 0){
    bool ok = TT->addBranch(word);
    word_count += ok ? 1 : 0;
    return ok;
  }else{
    return false;
  }
}

/**
* Removes a word from the dictionary
*/
bool Dictionary::hasWord(char* word)
{
  if(word && strlen(word) > 0){
    return TT->hasBranch(word);
  }else{
    return false;
  }
}

/**
* This is an iterator, returns the next word in the dictionary
* in alphabetical order
*/
char* Dictionary::getNextWord()
{
  return TT->getNextWord();
}

/**
* Indicates if the iterator still has words
* @return boolean
*/
bool Dictionary::hasNextWord()
{
  return TT->hasNextWord();
}

/**
* Resets the iterator
*/
void Dictionary::resetIterator()
{
  TT->resetIterator();
}

/**
* Returns the number of words stored in the dictionary
*/
int Dictionary::getWordCount()
{
  return word_count;
}

int Dictionary::getTotalNodeCount()
{
  return TT->tt_total_nodes;
}
