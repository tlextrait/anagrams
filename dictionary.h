/**
 * dictionary.h
 *
 * Thomas Lextrait
 * thomas.lextrait@gmail.com
 */

#include "trietree.h"

// Dictionary, must be one word per line, plain text
#define DICTIONARY_FILE "words.txt"

class Dictionary
{
public:
  Dictionary();
  void buildFromFile();
  bool addWord(char* word);
  bool hasWord(char* word);
  char* getNextWord();
  bool hasNextWord();
  void resetIterator();
  int getWordCount();
  int getTotalNodeCount();
private:
  int word_count;	// total number of words
  TrieTree* TT;		// TrieTree
};
