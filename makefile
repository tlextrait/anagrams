########################################
# Thomas Lextrait, thomas.lextrait@gmail.com
########################################
CXX=g++
FLAGS=-c -Wall
CXXFLAGS=-c -Wall -Wextra -pedantic -Werror
########################################

# Make everything
all: anagrams

# Make a fresh copy, clean up previous build
new: clean all

anagrams: anagrams.o dictionary.o trietree.o
		$(CXX) anagrams.o dictionary.o trietree.o -o anagrams

anagrams.o: anagrams.cpp anagrams.h dictionary.h
		$(CXX) $(FLAGS) anagrams.cpp dictionary.cpp

dictionary.o: dictionary.cpp dictionary.h trietree.h
		$(CXX) $(FLAGS) dictionary.cpp trietree.cpp

trietree.o: trietree.cpp trietree.h
		$(CXX) $(FLAGS) trietree.cpp

########################################
# Clean up
########################################
clean:
		rm -f *.o *.out anagrams test
